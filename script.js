let firstNum = prompt("Введіть перше число");

while (firstNum === null || firstNum === "" || isNaN(firstNum)) {
  firstNum = prompt("Введіть перше число", firstNum);
}

let secondNum = prompt("Введіть друге число");

while (secondNum === null || secondNum === "" || isNaN(secondNum)) {
  secondNum = prompt("Введіть друге число", secondNum);
}
let symbol = prompt(
  "Ввведіть математичну операцію, яку потрібно виконати: + , - , * або / ");

while (symbol === null || symbol === "" || !isNaN(symbol)) {
  symbol = prompt(
    "Ввведіть математичну операцію, яку потрібно виконати: + , - , * або / ", symbol);
}
let num1 = Number(firstNum);
let num2 = Number(secondNum);

function getResult(num1, num2, symbol) {

  switch (symbol) {
    case "+":
      return num1 + num2;
      
    case "-":
      return num1 - num2;
    case "*":
      return num1 * num2;
    case "/":
      if (num2 !== 0) {
        return num1 / num2;
      } else {
        return "Не ділится на нуль";
      }
    default:
      alert("Нічого рахувати");
  }
  
}
console.log(getResult(num1, num2, symbol));
